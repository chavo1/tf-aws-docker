# Terraform Lab
## Docker [ghost](https://docs.docker.com/samples/library/ghost/)
- Install [Vagrant](https://www.vagrantup.com/docs/installation/)
- Clone the repo
```
git clone https://gitlab.com/chavo1/tf-aws-docker.git
cd tf-aws-docker
vagrant up
sudo su -
cd /vagrant/docker
```
- Create workspaces 
```
terraform workspace new dev
Created and switched to workspace "dev"!
terraform workspace new prod
Created and switched to workspace "prod"!
```
- Start prod
```
terraform init
terraform plan
var.env
  env: dev or prod

  Enter a value: prod
terraform apply
var.env
  env: dev or prod

  Enter a value: prod
```
- Ghost prod is accessible on:
```
http://192.168.56.51/ghost/
```
- To destroy the environment
```
terraform destroy
```

