# Summary of "managing applications and infrastructure with terraform":

## Docker

We started with terraform installation than provider - Docker after we prepare the main.tf we saw that the plugin is on following directory:
```
.terraform/plugins/linux_amd64/terraform-provider-docker_v1.1.0_x4
```
We created and docker environment with docker ghost and with "terraform show" we have checked the exact containers:
```
module.image.docker_image.image_id:
id = sha256:74a5e05f88a12c0ee87e032fb5ac914e11e4ac2d89af1514f921a0eb548885eaghost:alpine
latest = sha256:74a5e05f88a12c0ee87e032fb5ac914e11e4ac2d89af1514f921a0eb548885ea
name = ghost:alpine
```
-   **Interpolation** was the next, it is a references on a variable or object within the infrastructure. For example we referenced an image into the docker container with needed ports.

-   **Taint** was next command it marks a Terraform resource as tainted, forcing it to be destroyed and recreated on the next apply.
-   **Terraform console** - we used it to take some values from the state file
```
>docker_container.container_id.name
blog
>docker_container.container_id.ip_addrss
172.17.0.2
```
-   **Output** with some values for example after terraform apply:
```
output "IP Address" {
value = "${module.container.ip}"
}
Output
IP Address = 172.17.0.2
```
-   **Variables** instead of hardcoded values we used a variables:
```
variable "image" {
description = "image for container"
}
Resource "docker_image" "image_id" {
name= "${var.image}"
}
```
-   We separated our variable and outputs as follow and saw that terraform looking in all *.tf files:
```
main.tf, variable.tf, output.tf
```
-   **Modules** - instead of big monolith file we have created a separate modules - image and container
```
root
└ main.tf  
└ variable.tf  
└ outputs.tf
└ terraform.tfvars
└ image  
   └ main.tf  
   └ variable.tf  
   └ outputs.tf
└ container
   └ main.tf  
   └ variable.tf  
   └ outputs.tf
```

-  We referenced our modules in root main.tf for example:
```
module "image" {
source = "./image"
Image = "${var.image}"
}
```
- **Map** was created to map the prod and dev environment for example in root main.tf and variables.tf:

```
module "image" {
source = "/image"
image = "${lookup(var.image, var.env)}"
}

variable "env" {
descrpition "env: dev or prod"
}

variable "image" {
descrpition "image for containers"
type = "map"
default {
dev = "ghost:latest"
prod = "ghost:alpine"
  }
}
```
- **Terraform workspaces** we deployed prod and dev environment from within a particular workspace each workspace contains its own .tfstate file:
```
terraform workspace new dev
Created and switched to workspace "dev"!
terraform workspace new prod
Created and switched to workspace "prod"!
```
- **Null resources and Local-Exec** we have created the output to be written on a file for example:
```
resource "null_resource" "null_id" {
provisioner "local-exec" {
command = "echo ${module.container.container_name}:${module.container.ip} >> container.txt"
  }
}
```
## AWS
- **Random Id** will generate a unique name for an S3 bucket that changes each time a new S3 bucket id is selected:
```
resource "random_id" "tf_bucket_id" {
   byte_length = 2
}
```
- **Count**  the following will going to create two AWS subnets for each count will be referenced our public_cidr:
```
resource "aws_subnet" "tf_public_subnet" {
	count = 2
	vpc_id = "${aws_vpc.tf_vpc.id}"
	cidr_block = "${var.public_cidr[count.index]}"
}
 ```
 - How to Reference a proper AMI for a particular region on a module:
 ```
 data "aws_ami" "server_ami" {
  most_recent = true

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn-ami-hvm*-x86_64-gp2"]
  }
}
```
- Stores the state as a given key in a given bucket on [Amazon S3](https://aws.amazon.com/s3/)<br/>
a. Create the bucket and assign it appropriate access permission and make a directry in this case "terraform"<br/>
 note: the name must be globally unique. 
```
terraform {
  backend "s3" {
    bucket = "mybucket"
        key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}
```
- Now we are ready to copy the existing state to the new backend:
```
terraform init
```
- After the initialization the backend will be on S3 bucket folder "terraform".
